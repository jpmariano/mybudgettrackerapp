import { useContext, useEffect } from 'react';
import { useRouter } from 'next/router';

// UserContext
import UserContext from '../../UserContext';

export default function index() {
	// router
	const router = useRouter();
	
	const { unsetUser } = useContext(UserContext);
	
	// this effect will run at least once just to clear the user's local storage
	useEffect(() => {
		unsetUser();
		
		router.push('/')
	}, []);
	
	return null;
}