import { Fragment, useState, useEffect } from 'react';
import moment from 'moment';

// React-Bootstrap
import { Row, Col, Form } from 'react-bootstrap';

// Component import
import Head from '../../components/Head';
import PieChart from '../../components/PieChart';

// helper
import AppHelper from '../../helpers/app-helper';

// Breakdown page
export default function Breakdown() {
	const headData = {
		title: 'Category Breakdown',
		description: 'Know the breakdown of your budget with this Pie Chart.'
	};
	
	// State
	const [records, setRecords] = useState([]);
	const [amountPerName, setAmountPerName] = useState([]);
	const [startDate, setStartDate] = useState(new Date());
	const [endDate, setEndDate] = useState(new Date());
	
	// Effect
	// To fetch the user's info
	useEffect(() => {
		fetch(`${AppHelper.API_URL}/users/details`, {
			headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }` }
		})
		.then(res => res.json())
		.then(user => {
			if (typeof user !== 'undefined') {
				setRecords(user.records);
			}
		});
	}, []);
	
	// Get the amount per record name
	useEffect(() => {
		let recordArr = [];
		
		records.forEach(element => {
			// start date should be less than or equal to the createdOn date and end date should be greater than or equal to the createdOn date
			if (moment(startDate).format('L') <= moment(element.createdOn).format('L') && moment(endDate).format('L') >= moment(element.createdOn).format('L')) {
				recordArr.push(element)
			}
		});
		
		setAmountPerName(recordArr);
	}, [startDate, endDate]);

	return (
		<Fragment>
			<Head dataProp={headData}/>
			<h2>Category Breakdown</h2>
			<Row>
				<Col>
					<Form>
						<Form.Group>
							<Form.Label>From</Form.Label>
							<Form.Control 
								type="date"
								value={ startDate }
								onChange={(e) => setStartDate(e.target.value)}
							/>
						</Form.Group>
					</Form>
				</Col>
				
				<Col>
					<Form>				
						<Form.Group>
							<Form.Label>To</Form.Label>
							<Form.Control 
								type="date"
								value={ endDate }
								onChange={(e) => setEndDate(e.target.value)}
							/>
						</Form.Group>
					</Form>
				</Col>
			</Row>
			<hr />
			<PieChart recordData={ amountPerName } />
		</Fragment>
	);
}