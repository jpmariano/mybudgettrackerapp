import { Fragment, useState, useEffect } from 'react';
import moment from 'moment';

// React-Bootstrap
import { Row, Col, Button, Card, Form, InputGroup } from 'react-bootstrap';

// Component import
import Head from '../../components/Head';

// helper
import AppHelper from '../../helpers/app-helper';

// Records page
export default function Records() {
	const headData = {
		title: 'Records',
		description: 'See your overall budget here.'
	};
	
	return (
		<Fragment>
			<Head dataProp={headData} />
			<Row>
				<Col>
					<h2>Records</h2>
					<RecordCards />
				</Col>
			</Row>
		</Fragment>
	);
}

const RecordCards = () => {
	const [recordsData, setRecordsData] = useState([]);
	const [searchTarget, setSearchTarget] = useState('');
	const [type, setType] = useState('');
	
	useEffect(() => {	
		fetch(`${AppHelper.API_URL}/users/details`, {
			headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }` }
		})
		.then(res => res.json())
		.then(user => {
			if (typeof user !== 'undefined') {
				// recordsArr will return all of the records of the user
				const recordsArr = user.records.map(record => {
					// to display all expenses
					if ((record.type === "Expense" && type === "Expense") && searchTarget === "") {
						return (
							<Card key={record._id} className="mb-3">
								<Card.Header>{moment(record.createdOn).format('D MMMM YYYY')}</Card.Header>
								<Card.Body>
									<Card.Title>{record.name}</Card.Title>
									<Card.Text>
										{record.type}
										<br />
										{record.description}
										<br />
										&#8369; {record.amount}
									</Card.Text>
									<Button variant="warning">
										Update
									</Button>
									<Button variant="danger" className="ml-2">
										Delete
									</Button>
								</Card.Body>
							</Card>
						);
						// to display all income
					} else if ((record.type === "Income" && type === "Income") && searchTarget === "") {
						return (
							<Card key={record._id} className="mb-3">
								<Card.Header>{moment(record.createdOn).format('D MMMM YYYY')}</Card.Header>
								<Card.Body>
									<Card.Title>{record.name}</Card.Title>
									<Card.Text>
										{record.type}
										<br />
										{record.description}
										<br />
										&#8369; {record.amount}
									</Card.Text>
									<Button variant="warning">
										Update
									</Button>
									<Button variant="danger" className="ml-2">
										Delete
									</Button>
								</Card.Body>
							</Card>
						);
						// if user typed the name of the record and is looking for an expense
					} else if ((record.name.toLowerCase() === searchTarget.toLowerCase()) && (type === "All" || type === "Expense") && record.type === "Expense") {
						return (
							<Card key={record._id} className="mb-3">
								<Card.Header>{moment(record.createdOn).format('D MMMM YYYY')}</Card.Header>
								<Card.Body>
									<Card.Title>{record.name}</Card.Title>
									<Card.Text>
										{record.type}
										<br />
										{record.description}
										<br />
										&#8369; {record.amount}
									</Card.Text>
									<Button variant="warning">
										Update
									</Button>
									<Button variant="danger" className="ml-2">
										Delete
									</Button>
								</Card.Body>
							</Card>
						);
						// if user typed the name of the record and is looking for an income
					} else if ((record.name.toLowerCase() === searchTarget.toLowerCase()) && (type === "All" || type === "Income") && record.type === "Income") {
						return (
							<Card key={record._id} className="mb-3">
								<Card.Header>{moment(record.createdOn).format('D MMMM YYYY')}</Card.Header>
								<Card.Body>
									<Card.Title>{record.name}</Card.Title>
									<Card.Text>
										{record.type}
										<br />
										{record.description}
										<br />
										&#8369; {record.amount}
									</Card.Text>
									<Button variant="warning">
										Update
									</Button>
									<Button variant="danger" className="ml-2">
										Delete
									</Button>
								</Card.Body>
							</Card>
						);
						// to display all
					} else if (type === "All" && searchTarget === "") {
						return (
							<Card key={record._id} className="mb-3">
								<Card.Header>{moment(record.createdOn).format('D MMMM YYYY')}</Card.Header>
								<Card.Body>
									<Card.Title>{record.name}</Card.Title>
									<Card.Text>
										{record.type}
										<br />
										{record.description}
										<br />
										&#8369; {record.amount}
									</Card.Text>
									<Button variant="warning">
										Update
									</Button>
									<Button variant="danger" className="ml-2">
										Delete
									</Button>
								</Card.Body>
							</Card>
						);
					}
				});
				
				setRecordsData(recordsArr);
			}
		});
	}, [type, searchTarget]);

	return (
		<Fragment>
			<InputGroup className="mb-3">
				<InputGroup.Prepend>
					<Button href="/records/create" variant="primary">
						Add New Record
					</Button>
				</InputGroup.Prepend>
				
				<Form.Control
					aria-describedby="basic-addon1"
					type="text"
					placeholder="Search your records"
					onChange={(e) => setSearchTarget(e.target.value)}
				/>
				
				<Form.Control
					as="select"
					onChange={(e) => setType(e.target.value)}
				>
					<option value="All">All</option>
					<option value="Expense">Expense</option>
					<option value="Income">Income</option>
				</Form.Control>
			</InputGroup>
			
			{ recordsData }
		</Fragment>
	);
};

// Record cards
// const RecordCards = () => {
// 	// State
// 	const [recordsData, setRecordsData] = useState([]);
// 	// const [filteredData, setFilteredData] = useState([]);
// 	const [searchTarget, setSearchTarget] = useState('');
// 	const [type, setType] = useState('');
// 	// const [name, setName] = useState('');
// 	// const [description, setDescription] = useState('');
// 	// const [amount, setAmount] = useState(0);
// 	// const [date, setDate] = useState('');
	
// 	// Effect
// 	useEffect(() => {	
// 		fetch(`${AppHelper.API_URL}/users/details`, {
// 			headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }` }
// 		})
// 		.then(res => res.json())
// 		.then(user => {
// 			if (typeof user !== 'undefined') {
// 				setRecordsData(user.records);
// 			}
// 		});
// 	}, []);
	
// 	// Returns all records for the user
// 	const allRecords = recordsData.map(record => {
// 		return (
// 			<Card key={record._id} className="mb-3">
// 				<Card.Header>{moment(record.createdOn).format('D MMMM YYYY')}</Card.Header>
// 				<Card.Body>
// 					<Card.Title>{record.name}</Card.Title>
// 					<Card.Text>
// 						{record.type}
// 						<br />
// 						{record.description}
// 						<br />
// 						{record.amount}
// 					</Card.Text>
// 					<Button variant="warning">
// 						Update
// 					</Button>
// 					<Button variant="danger" className="ml-2">
// 						Delete
// 					</Button>
// 				</Card.Body>
// 			</Card>
// 		);
// 	});
	
// 	function toSearch(e) {
// 		e.preventDefault();
		
// 		const matchingRecords = recordsData.map(record => {
// 			if (record.name.toLowerCase() === searchedRecord.toLowerCase()) {
// 				return (
// 					<Card key={record._id} className="mb-3">
// 						<Card.Header>{moment(record.createdOn).format('D MMMM YYYY')}</Card.Header>
// 						<Card.Body>
// 							<Card.Title>{record.name}</Card.Title>
// 							<Card.Text>
// 								{record.type}
// 								<br />
// 								{record.description}
// 								<br />
// 								{record.amount}
// 							</Card.Text>
// 							<Button variant="warning">
// 								Update
// 							</Button>
// 							<Button variant="danger" className="ml-2">
// 								Delete
// 							</Button>
// 						</Card.Body>
// 					</Card>
// 				);
// 			}
// 		});
		
// 		setFilteredData(matchingRecords);
// 	}

// 	return (
// 		<Fragment>
// 			<Form onSubmit={ toSearch }>
// 				<Form.Row>
// 					<Col xs="5" md="5">
// 						<Form.Group controlId="searchRecords">
// 							<Form.Control
// 								type="text"
// 								placeholder="Search your records"
// 								value={ searchTarget }
// 								onChange={(e) => setSearchTarget(e.target.value)}
// 							/>
// 						</Form.Group>
// 					</Col>
					
// 					<Col xs="5" md="5">
// 						<Form.Group controlId="filterByType">
// 							<Form.Control
// 								as="select"
// 								value={ type }
// 								onChange={(e) => setType(e.target.value)}
// 							>
// 								<option value="All">All</option>
// 								<option value="Income">Income</option>
// 								<option value="Expense">Expense</option>
// 							</Form.Control>
// 						</Form.Group>
// 					</Col>
					
// 					<Col xs="1" md="1">
// 						<Button variant="info" type="submit">
// 							Search
// 						</Button>
// 					</Col>
// 				</Form.Row>
// 			</Form>
			
// 			{ searchTarget !== '' ?
// 				<Fragment>
// 					{ filteredData }
// 				</Fragment>
// 				:
// 				<Fragment>
// 					{ allRecords }
// 				</Fragment>
// 			}
// 		</Fragment>
// 	);
// };